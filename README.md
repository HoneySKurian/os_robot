**GOAL**

In this repository, you may find the source code of our Operating system team project in EURECOM to build a robot for a "Relay Race".

Our robot is, **Bobert**.

The directory **FirstSession** contains the files used for the first assessment.

The directory **FinalRobot** contains the files used for the final assessment.


**TEAM WORKFLOW**

The team members are Rihab Harrak, Prashant Kumar Dey and Honey Susan Kurian. Just a small brief up about the work done. 

Before the first assessment on 13th December 2016, Rihab along with Honey worked on the construction of the robot. Prashant and Honey worked on the code. We did a lot of manipulation and tests with each of the wheels of the robot as they were not synchronised. 

After the first assessment, we were given new motors for the wheels. Hence, once we were back from Christmas holidays, we re-designed the robot and changed the code completely as the new wheels were synchronous.
Honey and Rihab worked on grabbing and dropping the ball, the robot movement as well as the content of the website. Prashant worked on communication with the server using bluetooth and on the website design. 

We didn't use any repository during coding as we used to sit together and code/discuss and the updated code was always on the robot. Hence, no timeline of commits can be observed.

**COMPILING AND LINKING**

In order to compile and link our project, we use a Makefile and then run it. The tachos and sensors need to be connected to the brick if you want the program to correctly execute.
The contents of the Makefile were :

all:
	gcc -pthread -I./ev3dev-c/source/ev3 -O2 -std=gnu99 -W -Wall -Wno-comment -c code.c -o code.o
	gcc -pthread code.o -Wall -lm -lev3dev-c -lbluetooth -o code

run:
	./updcode

To compile, we used:

make all

To run the code, we used :

make run *or* ./code(sometimes)


For more detailed information about the team contribution, the project report along with videos and pictures, you can access our website, http://robot.prashantdey.in/bobert/index.html