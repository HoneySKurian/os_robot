//Port C and Port D for Motors to move
//Right Motor - Port C - 67
//Left Motor - Port D - 68
//For Right turn, side value should be 0



#include <stdio.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
// WIN32 /////////////////////////////////////////
#ifdef __WIN32__

#include <windows.h>

#else

#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )

#endif

const char const *color[] = { "?", "BLACK", "BLUE", "YELLOW", "RED", "WHITE", "BROWN" };
#define COLOR_COUNT  (( int )( sizeof( color ) / sizeof( color[ 0 ])))


int forward_bobert(int k, int psp){
	int flag1 = 1;
	uint8_t sn;
	
	while (flag1<=k){
		Sleep(15);
		if ( ev3_search_tacho_plugged_in(67, 0, &sn, 0 )) {
		int max_speed;
		
		get_tacho_max_speed( sn, &max_speed );
		
		//set_tacho_stop_action_inx( sn, TACHO_HOLD );
		set_tacho_stop_action_inx( sn, TACHO_COAST );
		int speedo = 0;
		speedo = max_speed * 1/5;
		
		set_tacho_speed_sp( sn, speedo );
		//printf("\nPort = 67 and Speedo= %d", speedo );
		set_tacho_time_sp( sn, 0 );
		set_tacho_ramp_up_sp( sn, 0 );
		set_tacho_ramp_down_sp( sn, 0 );
		set_tacho_position_sp( sn, psp );
        set_tacho_command_inx( sn, TACHO_RUN_TO_REL_POS );
		printf("psp: %d\n", psp);
		//set_tacho_command_inx( sn, TACHO_RUN_TIMED );
		
	} else {
		printf( "LEGO_EV3_L_MOTOR 2 is NOT found\n" );
	}
				
	Sleep(10);
	if ( ev3_search_tacho_plugged_in(68, 0, &sn, 0 )) {
		int max_speed;
		
		//printf( "Port 68 working for %d... \n", flag1 );
		get_tacho_max_speed( sn, &max_speed );
		
		set_tacho_stop_action_inx( sn, TACHO_COAST );
		//set_tacho_stop_action_inx(sn, TACHO_HOLD);
		int speedo = 0;
		speedo = max_speed * 1/5;
		
		set_tacho_speed_sp( sn, speedo );
		//printf("\nPort = 68 and Speedo= %d", speedo );
		set_tacho_time_sp( sn, 0 );
		set_tacho_ramp_up_sp( sn, 0 );
		set_tacho_ramp_down_sp( sn, 0 );
		set_tacho_position_sp( sn, psp );
        set_tacho_command_inx( sn, TACHO_RUN_TO_REL_POS );
		//set_tacho_command_inx( sn, TACHO_RUN_TIMED );
	} else {
		printf( "LEGO_EV3_L_MOTOR 3 is NOT found\n" );
	}	
	
		
	flag1++;
		
	}

	return 0;
}


int backward_bobert(){
	
	int flag1 = 1;
	uint8_t sn;
	
	while (flag1<30){
		if ( ev3_search_tacho_plugged_in(67, 0, &sn, 0 )) {
		int max_speed;
		//printf("Port 67 working for %d ...\n", flag1 );
		get_tacho_max_speed( sn, &max_speed );
		
		//set_tacho_stop_action_inx( sn, TACHO_HOLD );
		set_tacho_stop_action_inx( sn, TACHO_HOLD );
		set_tacho_speed_sp( sn, -max_speed * 2 / 3 );
		set_tacho_time_sp( sn, 100 );
		set_tacho_ramp_up_sp( sn, 2000 );
		set_tacho_ramp_down_sp( sn, 2000 );
		set_tacho_command_inx( sn, TACHO_RUN_TIMED );
		
	} else {
		printf( "LEGO_EV3_L_MOTOR 2 is NOT found\n" );
	}
	if ( ev3_search_tacho_plugged_in(68, 0, &sn, 0 )) {
		int max_speed;
		Sleep(50);
		//printf( "Port 68 working for %d... \n", flag1 );
		get_tacho_max_speed( sn, &max_speed );
		
		//set_tacho_stop_action_inx( sn, TACHO_COAST );
		set_tacho_stop_action_inx(sn, TACHO_HOLD);
		set_tacho_speed_sp( sn, -max_speed * 2 / 3 );
		set_tacho_time_sp( sn, 100 );
		set_tacho_ramp_up_sp( sn, 2000 );
		set_tacho_ramp_down_sp( sn, 2000 );
		set_tacho_command_inx( sn, TACHO_RUN_TIMED );
	} else {
		printf( "LEGO_EV3_L_MOTOR 3 is NOT found\n" );
	}
	
	flag1++;
		
	}
	Sleep(1000);
	return 0;
	
}


int turn_90_degree(int port, int psp){
	
	int flag1 = 1,i;
	uint8_t sn;
	
	
		while (flag1<=15){
		if ( ev3_search_tacho_plugged_in(abs(port+1), 0, &sn, 0 )) {
		int max_speed;
		get_tacho_max_speed( sn, &max_speed );
		set_tacho_stop_action_inx( sn, TACHO_HOLD );
		set_tacho_speed_sp( sn, max_speed * 1 / 3 );
		set_tacho_time_sp( sn, 0 );
		set_tacho_ramp_up_sp( sn, 0 );
		set_tacho_ramp_down_sp( sn, 0 );
		set_tacho_position_sp( sn, psp );
        set_tacho_command_inx( sn, TACHO_RUN_TO_REL_POS );
		
		
	} else {
		printf( "LEGO_EV3_L_MOTOR 2 is NOT found\n" );
	}
	if ( ev3_search_tacho_plugged_in(abs(port), 0, &sn, 0 )) {
		int max_speed;
		//Sleep(15);
		//printf( "Port 68 working for %d... \n", flag1 );
		get_tacho_max_speed( sn, &max_speed );
		//printf("  max_speed = %d\n", max_speed );
		//printf("  sn = %d\n", sn );
		//set_tacho_stop_action_inx( sn, TACHO_COAST );
		set_tacho_stop_action_inx(sn, TACHO_HOLD);
		set_tacho_speed_sp( sn, max_speed * 1 / 3 );
		set_tacho_time_sp( sn, 0 );
		set_tacho_ramp_up_sp( sn, 0 );
		set_tacho_ramp_down_sp( sn, 0 );
		//set_tacho_command_inx( sn, TACHO_RUN_TIMED );
		set_tacho_position_sp( sn, -psp );
        set_tacho_command_inx( sn, TACHO_RUN_TO_REL_POS );
	} else {
		printf( "LEGO_EV3_L_MOTOR 3 is NOT found\n" );
	}
	
	flag1++;
			

}
 return 0;
}


int wheel(int port, int psp){
	
	uint8_t sn;
	FLAGS_T state;
	
		Sleep(15);
		if ( ev3_search_tacho_plugged_in(port, 0, &sn, 0 )) {
		int max_speed;
		
		get_tacho_max_speed( sn, &max_speed );
		
		set_tacho_stop_action_inx( sn, TACHO_HOLD );
		//set_tacho_stop_action_inx( sn, TACHO_COAST );
		
		printf("wheel psp: %d\n", psp);
		
		set_tacho_speed_sp( sn, max_speed * 1 / 10 );
		//printf("\nPort = 67 and Speedo= %d", speedo );
		set_tacho_time_sp( sn, 0 );
		set_tacho_ramp_up_sp( sn, 0 );
		set_tacho_ramp_down_sp( sn, 0 );
		set_tacho_position_sp( sn, psp );
        set_tacho_command_inx( sn, TACHO_RUN_TO_REL_POS );
		
		//set_tacho_command_inx( sn, TACHO_RUN_TIMED );
		
	} else {
		printf( "LEGO_EV3_L_MOTOR 2 is NOT found\n" );
	}
				
	
	
		


	return 0;
	
	
}







int main( void )
{
	int i,flag1=1,gyro_counter=1;
	int gyro_odd, gyro_exact;
	uint8_t sn;
	FLAGS_T state;
	uint8_t sn_touch;
	uint8_t sn_color;
	uint8_t sn_compass;
	uint8_t sn_sonar;
	uint8_t sn_mag;
	char s[ 256 ];
	int val, counter=0, color_counter = 1,take;
	float value, diameter = 5.45;
	uint32_t n, ii;
#ifndef __ARM_ARCH_4T__
	/* Disable auto-detection of the brick (you have to set the correct address below) */
	ev3_brick_addr = "169.254.223.200";

#endif
	if ( ev3_init() == -1 ) return ( 1 );

#ifndef __ARM_ARCH_4T__
	printf( "The EV3 brick auto-detection is DISABLED,\nwaiting %s online with plugged tacho...\n", ev3_brick_addr );

#else
	printf( "Waiting tacho is plugged...\n" );

#endif
	while ( ev3_tacho_init() < 1 ) Sleep( 1000 );

	printf( "*** ( EV3 ) Hello! ***\n" );

	printf( "Found tacho motors:\n" );
	for ( i = 0; i < DESC_LIMIT; i++ ) {
		if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
			printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
			printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
			printf("  port = %d %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i));
		}
	}
	
	
	
	Sleep(4000);
	//Run all sensors
	ev3_sensor_init();
	
	printf( "Found sensors:\n" );
	for ( i = 0; i < DESC_LIMIT; i++ ) {
		if ( ev3_sensor[ i ].type_inx != SENSOR_TYPE__NONE_ ) {
			printf( "  type = %s\n", ev3_sensor_type( ev3_sensor[ i ].type_inx ));
			printf( "  port = %s\n", ev3_sensor_port_name( i, s ));
			if ( get_sensor_mode( i, s, sizeof( s ))) {
				printf( "  mode = %s\n", s );
			}
			if ( get_sensor_num_values( i, &n )) {
				for ( ii = 0; ii < n; ii++ ) {
					if ( get_sensor_value( ii, i, &val )) {
						printf( "  value%d = %d\n", ii, val );
					}
				}
			}
		}
	}
	if ( ev3_search_sensor( LEGO_EV3_TOUCH, &sn_touch, 0 )) {
		printf( "TOUCH sensor is found, press BUTTON for EXIT...\n" );
	} 
	int control = 1, control_loop_counter = 1,  stop = 1;
	while (stop){
		
	    	
	    	
		if (ev3_search_sensor(LEGO_EV3_GYRO, &sn_mag,0)){
			printf("Magnetic sensor found, reading magnet...\n");
			if ( !get_sensor_value0(sn_mag, &value )) {
				value = 0;
			}
			if (gyro_counter==1){
				gyro_exact = value;
			}
			
			else{
				
				gyro_odd = value;
			}
			
		if((gyro_counter>=2)){
				
				if ((abs(gyro_exact - gyro_odd)!=0) && (abs(gyro_exact - gyro_odd)<=2)){
					if ((gyro_exact - gyro_odd) < 0){
					Sleep(1000);
					printf("Port 67 and value is: %d \n", (gyro_exact - gyro_odd));
					wheel(67, 21);
					}
					if ((gyro_exact - gyro_odd)>0){
					Sleep(1000);
					printf("Port 67 and value is: %d \n", (gyro_exact - gyro_odd));
					wheel(67,-20);
					}
								
				}
				if ((abs(gyro_exact - gyro_odd)>2) && (abs(gyro_exact - gyro_odd)<=4)){
					if ((gyro_exact - gyro_odd) < 0){
					Sleep(1000);
					printf("Port 67 and value is: %d \n", (gyro_exact - gyro_odd));
					wheel(67, 33);
					}
					if ((gyro_exact - gyro_odd)>0){
					Sleep(1000);
					printf("Port 68 and value is: %d \n", (gyro_exact - gyro_odd));
					wheel(67,-25);
					}
					
								
				}
				
					if ((abs(gyro_exact - gyro_odd)>4) && (abs(gyro_exact - gyro_odd)<=6)){
					if ((gyro_exact - gyro_odd) < 0){
					Sleep(1000);
					printf("Port 67 and value is: %d \n", (gyro_exact - gyro_odd));
					wheel(67, 43);
					}
					if ((gyro_exact - gyro_odd)>0){
					Sleep(1000);
					printf("Port 68 and value is: %d \n", (gyro_exact - gyro_odd));
					wheel(67,-29);
					}
								
				}
				
				if (abs(gyro_exact - gyro_odd) > 7){
					if ((gyro_exact - gyro_odd)<0){
					Sleep(1000);
					printf("Port 67 and value is: %d \n", (gyro_exact - gyro_odd));
					wheel(67,52);
					}
					if ((gyro_exact - gyro_odd)>0){
					Sleep(1000);
					printf("Port 68 and value is: %d \n", (gyro_exact - gyro_odd));
					wheel(67,-32);
					}				
				}
				
			}
			
			printf( "\r(%f) \n", value);
			gyro_counter = gyro_counter+1;
			
			fflush( stdout );
	    	}
		
		
	
	
		
		
			
		if (control==1 || control == 3){
			if (ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0)){
			printf("SONAR found, reading sonar...\n");
			if ( !get_sensor_value0(sn_sonar, &value )) {
				value = 0;
			}
			
			printf( "\r(%f) \n", value);
			fflush( stdout );
	    	}
			
			
			if(value>=400){
			Sleep(2000);
			forward_bobert(1, 360);
			Sleep(2000);
			
		}
		
		if(value<400&&value>=120){
			take = (value*45)/180;
			Sleep(2000);
			
			forward_bobert(1,take);
			Sleep(100);
		}
		if(value<340){
			take = value/10;
			Sleep(2000);
			if (control == 1){
				Sleep(1000);
				turn_90_degree(-68, 33);	
				gyro_counter=1;
				control++;
				Sleep(2000);
			}
			if (control == 3){
				Sleep(1000);
				turn_90_degree(67, 100);	
				gyro_counter=1;
				control++;
				Sleep(1000);
				
			}
			
			
			
		}		
			
		}
		
		if (control==2 || control == 4 || control ==5){
			
			if(control == 2){
				Sleep(2000);
				if (control_loop_counter<=4){
					forward_bobert(1, 260);
					Sleep(1000);
									
				}
				if (control_loop_counter==5){
					turn_90_degree(67, 110);
					gyro_counter=1;
					Sleep(1000);
					control++;
					
				}
								
			}
			
			if(control == 4){
				Sleep(2000);
				if (control_loop_counter>5 && control_loop_counter<=10){
					forward_bobert(1, 260);
					Sleep(1000);
									
				}
				if (control_loop_counter==11){
					turn_90_degree(-68, 35);
					gyro_counter=1;
					Sleep(1000);
					control++;
					
				}				
				
			}
			
			if(control ==5){
				if (control_loop_counter>10 && control_loop_counter<=22){
					forward_bobert(1, 300);
					Sleep(1000);
					}	
				if (control_loop_counter==20){
					stop =0;
					printf("[+] Bobert Stopping");
				}
									
				
				
			}
			control_loop_counter++;
			
		}
			
		
	}
		
			
			
	
	ev3_uninit();
	printf( "*** ( EV3 ) Bye! ***\n" );

	return ( 0 );
}



