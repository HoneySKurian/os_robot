#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>

#ifdef _WIN32
#include <Windows.h>    
#else
#include <unistd.h>
#define Sleep(msec) usleep((msec) * 1000)
#endif

#define portA 65
#define portB 66
#define portC 67
#define portD 68

// 0 = Big arena; 1 = Small arena
#define ARENA 1
#if ARENA == 0
    #define BORDER_X_MAX 200
    #define BORDER_Y_MAX 400
#else
    #define BORDER_X_MAX 120
    #define BORDER_Y_MAX 200
#endif

#define WHEEL_RADIUS 5
#define MARGIN 160
#define GOTOFIN 100
#define DIST_ROT 18

#define SERV_ADDR   "24:FD:52:F9:AB:76"
#define TEAM_ID     1 
#define MSG_ACK     0
#define MSG_NEXT    1
#define MSG_START   2
#define MSG_STOP    3
#define MSG_CUSTOM  4
#define MSG_KICK    5
#define MSG_POSITION 6
#define MSG_BALL    7

#define BALL_PICKED_UP 1
#define BALL_DROPPED 0 


typedef struct _pos_t{
    int32_t x;
    int32_t y;
} pos_t;

typedef struct _act_val_t{
    int angle;
    int dist;
    int speed;
} act_val_t;


pos_t bobert;
act_val_t act_val;
int s;

unsigned char rank = 0;
unsigned char length = 0;
unsigned char previous = 0xFF;
unsigned char ally = 0xFF;
unsigned char side=0;

uint16_t msgId = 0;

/*SMALL ARENA*/

/*Moves Bobert forward, backward, left and right*/
int moveBobert(uint8_t snr, uint8_t snl, int rot_speed, int mtimes, int rtimes, int ltimes, int next, int amount){
    set_tacho_command_inx( snr, TACHO_RESET );
    set_tacho_command_inx( snl, TACHO_RESET);
    set_tacho_speed_sp( snr, rot_speed);
    set_tacho_speed_sp( snl, rot_speed);
    int newspeed = rot_speed *mtimes;
    set_tacho_position_sp( snr, newspeed);
    set_tacho_position_sp( snl, newspeed);
    set_tacho_command_inx( snr, TACHO_RUN_TO_REL_POS );
    set_tacho_command_inx( snl, TACHO_RUN_TO_REL_POS );
    Sleep(abs(mtimes*1000));
    act_val.speed = newspeed;
    act_val.angle = 0;
    if(next == 0){
        set_tacho_stop_action_inx( snr, TACHO_HOLD );
        set_tacho_stop_action_inx( snl, TACHO_HOLD);
        if(rtimes >0){
            set_tacho_position_sp( snr, ((rot_speed+amount) * rtimes));
            set_tacho_position_sp( snl, ((rot_speed+amount) * ltimes));
            act_val.angle = -90;
        }
        else if(ltimes > 0){
            set_tacho_position_sp( snr, ((rot_speed+amount) * rtimes));
            set_tacho_position_sp( snl, ((rot_speed+amount) * ltimes));
            act_val.angle = 90;
        }
        else{
            set_tacho_position_sp( snr, (rot_speed * rtimes));
            set_tacho_position_sp( snl, (rot_speed * ltimes));
        }

        set_tacho_command_inx( snr, TACHO_RUN_TO_REL_POS );
        set_tacho_command_inx( snl, TACHO_RUN_TO_REL_POS );
        set_tacho_stop_action_inx( snr, TACHO_HOLD );
        set_tacho_stop_action_inx( snl, TACHO_HOLD);
    }
    return 0;
}

/*Drops the ball and picks the ball*/
int DropAndPick(uint8_t snh, uint8_t snt ,int hand_speed, int teeth_speed, int htimes, int ttimes){

    set_tacho_command_inx( snh, TACHO_RESET );
    set_tacho_command_inx( snt, TACHO_RESET );
    set_tacho_speed_sp( snh, hand_speed );
    set_tacho_speed_sp( snt, teeth_speed );

    set_tacho_position_sp( snh, hand_speed * htimes );
    set_tacho_command_inx( snh, TACHO_RUN_TO_REL_POS );
    Sleep(2000);
    if(ttimes > 0){
        set_tacho_position_sp( snt, (teeth_speed-400) *ttimes);
        set_tacho_command_inx( snt, TACHO_RUN_TO_REL_POS );
        Sleep(2000);
        set_tacho_position_sp( snt, (teeth_speed) *ttimes );
        set_tacho_command_inx( snt, TACHO_RUN_TO_REL_POS );
        Sleep(abs(ttimes*1000));
    }
    else if(ttimes < 0){
        set_tacho_position_sp( snt, (teeth_speed) *ttimes );
        set_tacho_command_inx( snt, TACHO_RUN_TO_REL_POS );
        Sleep(abs(ttimes*1000));
    }

    set_tacho_position_sp( snh, -(hand_speed+50) *htimes);
    set_tacho_command_inx( snh, TACHO_RUN_TO_REL_POS );

    return 0;
}


/*Compute distance to the target and angle to turn*/
void get_target_position (pos_t target_pos, int *dist, int *angle){

    /*compute the angle*/
    double arg;
    arg=1.0*(target_pos.y - bobert.y)/(target_pos.x - bobert.x);  
    *angle= (int)(90-(atan(arg)*180/M_PI));

    /*compute the distance*/ 
    *dist=(int)sqrt((target_pos.x - bobert.x)*(target_pos.x - bobert.x) + (target_pos.y - bobert.y)*(target_pos.y - bobert.y)); 
}

/*Get the target position and move towards it*/
int go_to_target(pos_t target_pos, uint8_t snr, uint8_t snl, uint8_t snh, uint8_t snt, int rotation, int next){
    int times;
    //Know where we are going 
    get_target_position(target_pos, &(act_val.dist), &(act_val.angle));
    times = act_val.dist/DIST_ROT;
    //Shift!    
    if(act_val.angle > 0){
        moveBobert(snr, 0 , (act_val.angle+MARGIN), 1, 0, 0, next+1, 0);
    }
    else{
        moveBobert(0, snl, (-act_val.angle+MARGIN), 1, 0, 0, next+1, 0);    
    }
       //Go
    Sleep(2000);
    moveBobert(snr, snl, rotation, 1, 0, 0, next+1, 0);

        //DropAndPick(snh, snt, HAND, TEETH);
        //Sleep(8000);
        //moveBobert(snr, snl, rotation, -2);

        //update position
    bobert.x = bobert.x + act_val.dist*sin(act_val.angle*M_PI/180); 
    bobert.y = bobert.y + act_val.dist*cos(act_val.angle*M_PI/180);
    target_pos.x = 15;
    target_pos.y = 180;


        /*get_target_position(target_pos, &(act_val.dist), &(act_val.angle));
        printf("%d %d\n", act_val.angle, act_val.dist);
        times = act_val.dist/DIST_ROT;
        printf("TImes :%d\n", times);
        if(act_val.angle > 0){
            moveBobert(0, snl , (act_val.angle-GOTOFIN), 1);
        }
        else{
            moveBobert(snr, 0, (-act_val.angle-GOTOFIN), 1);    
        }

        Sleep(1000);
        moveBobert(snr, snl, rotation, times+1);*/

        return 0;
}

/*Updating the position coordinates to send to server*/
void update_pos(int32_t delta_time){
    bobert.x=bobert.x+(2*WHEEL_RADIUS*M_PI*act_val.speed)*sin(act_val.angle*M_PI/180); 
    bobert.y=bobert.y+(2*WHEEL_RADIUS*M_PI*act_val.speed)*cos(act_val.angle*M_PI/180);
}

/*Beginner is in the right and is going to drop the ball*/
void *beginner_right(){
    //Initialisations
    uint8_t sn_compass, sn_sonar, sn_gyro;
    uint8_t snr, snl, snh, snt;
    float gyro_value, sonar_value;
    pos_t target_pos;
    int rotation, hand, teeth, next,turning;
    
    //Search tachos
    while ( ev3_tacho_init() < 1 ) Sleep(1000);
    ev3_search_tacho_plugged_in(portA, 0, &snh, 0 );
    ev3_search_tacho_plugged_in(portB, 0, &snt, 0 );
    ev3_search_tacho_plugged_in(portC, 0, &snr, 0 );
    ev3_search_tacho_plugged_in(portD, 0, &snl, 0 );
    
    //Search sensors
    while ( ev3_sensor_init() < 1 ) Sleep(1000);
    ev3_search_sensor(HT_NXT_COMPASS, &sn_compass,0);
    ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0);
    ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro,0);
    set_sensor_mode(sn_gyro, "GYRO-ANG");
    Sleep(1000);
    set_sensor_mode(sn_gyro, "GYRO-G&A");
    Sleep(1000);
    get_sensor_value0(sn_gyro, &gyro_value);
    get_sensor_value0(sn_sonar, &sonar_value);

    rotation = 360;
    next = 0;
    hand = 80;
    teeth = 900;

    moveBobert(snr, snl, rotation, 3, 1, 0, next, 100);
    Sleep(3000);
    get_sensor_value0(sn_sonar, &sonar_value ); 
    printf( "\r(%f) \n", sonar_value);
    moveBobert(snr, snl, rotation, 1, 0, 0, next+1, 0);
    Sleep(2000);
    DropAndPick(snh, snt, hand, teeth, 1, 1);
    Sleep(2000);
    moveBobert(snr, snl, rotation, -1, 0, -1, next, 150);
    Sleep(2000);
    turning = 0;
    while(turning < 3){
        moveBobert(snr, snl, rotation, 0, 1, -1, next, 0);
        turning = turning +1;
        printf("%d\n", turning );
    }
    set_tacho_command_inx( snr, TACHO_RESET );
    set_tacho_command_inx( snl, TACHO_RESET);
    Sleep(2000);
    get_sensor_value0(sn_sonar, &sonar_value ); 
    printf( "\r(%f) \n", sonar_value);
    moveBobert(snr, snl, rotation, -3, 0, 0, next+1, 0);
    Sleep(1000);
}

/*Beginner started from left, picked the ball and this function helps to go drop the ball and go to Beginner right position*/
void* beginner_left_dropball(){
    //Initialisations
    uint8_t sn_compass, sn_sonar, sn_gyro;
    uint8_t snr, snl, snh, snt;
    float gyro_value, sonar_value;
    pos_t target_pos;
    int rotation, hand, teeth, next,turning;

    //Search tachos
    while ( ev3_tacho_init() < 1 ) Sleep(1000);
    ev3_search_tacho_plugged_in(portA, 0, &snh, 0 );
    ev3_search_tacho_plugged_in(portB, 0, &snt, 0 );
    ev3_search_tacho_plugged_in(portC, 0, &snr, 0 );
    ev3_search_tacho_plugged_in(portD, 0, &snl, 0 );

    //Search sensors
    while ( ev3_sensor_init() < 1 ) Sleep(1000);
    ev3_search_sensor(HT_NXT_COMPASS, &sn_compass,0);
    ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0);
    ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro,0);
    set_sensor_mode(sn_gyro, "GYRO-ANG");
    Sleep(1000);
    set_sensor_mode(sn_gyro, "GYRO-G&A");
    Sleep(1000);
    get_sensor_value0(sn_gyro, &gyro_value);
    get_sensor_value0(sn_sonar, &sonar_value);

    rotation = 360;
    next = 0;
    hand = 80;
    teeth = 900;

    moveBobert(snr, snl, rotation, 3, 1, 0, next, 50);
    Sleep(3000);
    if (ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0)){
        get_sensor_value0(sn_sonar, &sonar_value ); 
        printf( "\r(%f) \n", sonar_value);
        if((sonar_value>745) && (sonar_value<780)){
            moveBobert(snr, snl, 180, 1, 0, 0, next+1, 0);
            Sleep(2000);
        }
        if(sonar_value>780){
            moveBobert(snr, snl, rotation, 1, 0, 0, next+1, 0);
            Sleep(2000);
        }
    }

    DropAndPick(snh, snt, hand, teeth, 1, 1);
    Sleep(2000);
    moveBobert(snr, snl, rotation, -1, 0, -1, next, 180);
    Sleep(2000);
    turning = 0;
    while(turning < 3){
        moveBobert(snr, snl, rotation, 0, 1, -1, next, 0);
        turning = turning +1;
        printf("%d\n", turning );
    }
    set_tacho_command_inx( snr, TACHO_RESET );
    set_tacho_command_inx( snl, TACHO_RESET);
    Sleep(2000);
    get_sensor_value0(sn_sonar, &sonar_value ); 
    printf( "\r(%f) \n", sonar_value);
    moveBobert(snr, snl, rotation, -3, 0, 0, next+1, 0);
    Sleep(1000);
}

/*Beginner dropped ball and is waiting in the left for Finisher to perform*/
void* beginner_left(){
    //Initialisations
    uint8_t sn_compass, sn_sonar, sn_gyro;
    uint8_t snr, snl, snh, snt;
    float gyro_value, sonar_value;
    pos_t target_pos;
    int rotation, hand, teeth, next, flag, forward, ball, direction, turning;

    //Search tachos
    while ( ev3_tacho_init() < 1 ) Sleep(1000);
    ev3_search_tacho_plugged_in(portA, 0, &snh, 0 );
    ev3_search_tacho_plugged_in(portB, 0, &snt, 0 );
    ev3_search_tacho_plugged_in(portC, 0, &snr, 0 );
    ev3_search_tacho_plugged_in(portD, 0, &snl, 0 );

    //Search sensors
    while ( ev3_sensor_init() < 1 ) Sleep(1000);
    ev3_search_sensor(HT_NXT_COMPASS, &sn_compass,0);
    ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0);
    ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro,0);
    set_sensor_mode(sn_gyro, "GYRO-ANG");
    Sleep(1000);
    set_sensor_mode(sn_gyro, "GYRO-G&A");
    Sleep(1000);
    get_sensor_value0(sn_gyro, &gyro_value);
    next = 0;
    bobert.x = 28;
    bobert.y = 32;
    target_pos.x = 58;
    target_pos.y = 91;
    rotation = 360;
    //go_to_target(target_pos, snr, snl, snh, snt,rotation, next);

    hand = 80;
    teeth = 250;
    ball = 1;

    moveBobert(snr, snl, rotation, 3, 0, 1, next, 100);
    Sleep(3000);

    //Using Sonar to search for ball--GRABBING STARTS
    if (ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0)){
        get_sensor_value0(sn_sonar, &sonar_value );
        while(ball){
            flag = 0;
            forward = 0;
            while(flag < 10 && forward <2 && ball == 1){
                //Rotating to right
                moveBobert(snr, snl, rotation, 0, -1, 1, next, 0);
                get_sensor_value0(sn_sonar, &sonar_value ); 
                float pastSonarValue = 0.0; 
                if (flag>0){
                    if ((abs(pastSonarValue-sonar_value)<450) && (abs(pastSonarValue-sonar_value)>120)){
                        set_tacho_command_inx( snr, TACHO_RESET );
                        set_tacho_command_inx( snl, TACHO_RESET);
                        moveBobert(snr, snl, 100, 1, 0, 0, next+1, 0);
                        set_tacho_command_inx( snr, TACHO_RESET );
                        set_tacho_command_inx( snl, TACHO_RESET);
                        forward = forward + 1;
                    }
                    if (sonar_value<118){
                        set_tacho_command_inx( snr, TACHO_RESET );
                        set_tacho_command_inx( snl, TACHO_RESET);
                        DropAndPick(snh, snt, hand, teeth, 1, -6);
                        flag=10;
                        direction = 0;
                        ball = 0;
                    }

                }
                pastSonarValue= sonar_value;
                if(flag == 9){
                    set_tacho_command_inx( snr, TACHO_RESET );
                    set_tacho_command_inx( snl, TACHO_RESET);
                }
                flag = flag + 1;
            }

            flag = 0;
            forward = 0;

            while(flag < 10 && forward <2 && ball ==1){
                //Rotating to left
                moveBobert(snr, snl, rotation, 0, 1, -1, next, 0);
                get_sensor_value0(sn_sonar, &sonar_value ); 
                float pastSonarValue = 0.0; 
                if (flag>0){
                    if ((abs(pastSonarValue-sonar_value)<200) && (abs(pastSonarValue-sonar_value)>120)){
                        set_tacho_command_inx( snr, TACHO_RESET );
                        set_tacho_command_inx( snl, TACHO_RESET);
                        moveBobert(snr, snl, 100, 1, 0, 0, next+1, 0);
                        set_tacho_command_inx( snr, TACHO_RESET );
                        set_tacho_command_inx( snl, TACHO_RESET);
                        forward = forward + 1;
                    }
                    if (sonar_value<120){
                        set_tacho_command_inx( snr, TACHO_RESET );
                        set_tacho_command_inx( snl, TACHO_RESET);
                        DropAndPick(snh, snt, hand, teeth, 1, -6);
                        flag=10;
                        direction = 1;
                        ball = 0;
                    }
                }
                pastSonarValue= sonar_value;
                if(flag == 9){
                    set_tacho_command_inx( snr, TACHO_RESET );
                    set_tacho_command_inx( snl, TACHO_RESET);
                }
                flag = flag + 1;
            }
        }
        set_tacho_command_inx( snr, TACHO_RESET );
        set_tacho_command_inx( snl, TACHO_RESET);
        Sleep(2000);
        int findstraight = 0;

        while(findstraight < 10){
            get_sensor_value0(sn_sonar, &sonar_value ); 
            if(direction == 0){ //robot is on the left
                if((sonar_value<735) && (sonar_value>720)){
                    moveBobert(snr, snl, 150, 0, 1, -1, next, 0);
                }
                if(sonar_value>735){
                    moveBobert(snr, snl, 150, 0, 1, -1, next, 0);
                }
                if((sonar_value<719) && (sonar_value>703)){ //straight
                    findstraight = 11;
                }
                if((sonar_value<703)){
                    moveBobert(snr, snl, 100, -1, 0, 0, next+1, 0);
                }
                set_tacho_command_inx( snr, TACHO_RESET );
                set_tacho_command_inx( snl, TACHO_RESET);

            }

            else if(direction == 1){ //robot is on the right
                if((sonar_value<735) && (sonar_value>720)){
                    moveBobert(snr, snl, 150, 0, -1, 1, next, 0);
                }
                if(sonar_value>735){
                    moveBobert(snr, snl, 150, 0, -1, 1, next, 0);
                }
                if((sonar_value<719) && (sonar_value>703)){ //straight
                    findstraight = 11;
                }
                if((sonar_value<703)){
                    moveBobert(snr, snl, 100, -1, 0, 0, next+1, 0);
                }
                set_tacho_command_inx( snr, TACHO_RESET );
                set_tacho_command_inx( snl, TACHO_RESET);
            }
            findstraight = findstraight + 1;
        }

        fflush( stdout );
    }
    Sleep(2000);
    moveBobert(snr, snl, 300, -1, 0, 0, next+1, 0);
    set_tacho_command_inx( snr, TACHO_RESET );
    set_tacho_command_inx( snl, TACHO_RESET);
    Sleep(2000);        
    moveBobert(snr, snl, rotation, 0, 0, 1, next, 150);
    get_sensor_value0(sn_sonar, &sonar_value ); 
    Sleep(2000);
    while(turning < 4){
        moveBobert(snr, snl, rotation, 0, 1, -1, next, 0);
        turning = turning +1;
    }
    set_tacho_command_inx( snr, TACHO_RESET );
    set_tacho_command_inx( snl, TACHO_RESET);
    Sleep(2000);
    get_sensor_value0(sn_sonar, &sonar_value ); 
    moveBobert(snr, snl, rotation, -4, 0, 0, next+1, 0);

    Sleep(3000);
    beginner_left_dropball();
    Sleep(2000);
}

/*Finisher is going to drop the ball after picking it first*/
void* finisher_left(){
    //Initialisations
    uint8_t sn_compass, sn_sonar, sn_gyro;
    uint8_t snr, snl, snh, snt;
    float gyro_value, sonar_value;
    pos_t target_pos;
    int rotation, hand, teeth, next,turning;
    
    //Search tachos
    while ( ev3_tacho_init() < 1 ) Sleep(1000);
    ev3_search_tacho_plugged_in(portA, 0, &snh, 0 );
    ev3_search_tacho_plugged_in(portB, 0, &snt, 0 );
    ev3_search_tacho_plugged_in(portC, 0, &snr, 0 );
    ev3_search_tacho_plugged_in(portD, 0, &snl, 0 );
    
    //Search sensors
    while ( ev3_sensor_init() < 1 ) Sleep(1000);
    ev3_search_sensor(HT_NXT_COMPASS, &sn_compass,0);
    ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0);
    ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro,0);
    set_sensor_mode(sn_gyro, "GYRO-ANG");
    Sleep(1000);
    set_sensor_mode(sn_gyro, "GYRO-G&A");
    Sleep(1000);
    get_sensor_value0(sn_gyro, &gyro_value);
    get_sensor_value0(sn_sonar, &sonar_value);

    rotation = 360;
    next = 0;
    hand = 80;
    teeth = 900;

    moveBobert(snr, snl, rotation, 3, 0, 1, next, 50);
    Sleep(3000);
    if (ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0)){
        get_sensor_value0(sn_sonar, &sonar_value ); 
        if(sonar_value>745){
            moveBobert(snr, snl, rotation, 1, 0, 0, next+1, 0);
            Sleep(2000);
        }
    }
    
    DropAndPick(snh, snt, hand, teeth, 1, 1);
    Sleep(2000);
    moveBobert(snr, snl, rotation, -1, -1, 0, next, 150);
    Sleep(2000);
    turning = 0;
    while(turning < 3){
        moveBobert(snr, snl, rotation, 0, -1, 1, next, 0);
        turning = turning +1;
    }
    set_tacho_command_inx( snr, TACHO_RESET );
    set_tacho_command_inx( snl, TACHO_RESET);
    Sleep(2000);
    get_sensor_value0(sn_sonar, &sonar_value ); 
    moveBobert(snr, snl, rotation, -4, 0, 0, next+1, 0);
    Sleep(1000);
    //finisher();

    }

/*Finisher is on the right and is going to pick the ball*/
void* finisher_right(){
    //Initialisations
    uint8_t sn_compass, sn_sonar, sn_gyro;
    uint8_t snr, snl, snh, snt;
    float gyro_value, sonar_value;
    pos_t target_pos;
    int rotation, hand, teeth, next, flag, forward, ball, direction, turning;

    //Search tachos
    while ( ev3_tacho_init() < 1 ) Sleep(1000);
    ev3_search_tacho_plugged_in(portA, 0, &snh, 0 );
    ev3_search_tacho_plugged_in(portB, 0, &snt, 0 );
    ev3_search_tacho_plugged_in(portC, 0, &snr, 0 );
    ev3_search_tacho_plugged_in(portD, 0, &snl, 0 );
    
    //Search sensors
    while ( ev3_sensor_init() < 1 ) Sleep(1000);
    ev3_search_sensor(HT_NXT_COMPASS, &sn_compass,0);
    ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0);
    ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro,0);
    set_sensor_mode(sn_gyro, "GYRO-ANG");
    Sleep(1000);
    set_sensor_mode(sn_gyro, "GYRO-G&A");
    Sleep(1000);
    get_sensor_value0(sn_gyro, &gyro_value);
    next = 0;
    bobert.x = 28;
    bobert.y = 32;
    target_pos.x = 58;
    target_pos.y = 91;
    rotation = 360;
    //go_to_target(target_pos, snr, snl, snh, snt,rotation, next);
    
    hand = 80;
    teeth = 250;
    ball = 1;
    
    moveBobert(snr, snl, rotation, 3, 1, 0, next, 100);
    Sleep(3000);

    //Using Sonar to search for ball--GRABBING STARTS
    if (ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0)){
        get_sensor_value0(sn_sonar, &sonar_value );
        while(ball){
            flag = 0;
            forward = 0;
            while(flag < 10 && forward <2 && ball == 1){
                //Rotating to right
                moveBobert(snr, snl, rotation, 0, -1, 1, next, 0);
                get_sensor_value0(sn_sonar, &sonar_value ); 
                float pastSonarValue = 0.0; 
                if (flag>0){
                    if ((abs(pastSonarValue-sonar_value)<450) && (abs(pastSonarValue-sonar_value)>120)){
                        set_tacho_command_inx( snr, TACHO_RESET );
                        set_tacho_command_inx( snl, TACHO_RESET);
                        moveBobert(snr, snl, 100, 1, 0, 0, next+1, 0);
                        set_tacho_command_inx( snr, TACHO_RESET );
                        set_tacho_command_inx( snl, TACHO_RESET);
                        forward = forward + 1;
                    }
                    if (sonar_value<105){
                        set_tacho_command_inx( snr, TACHO_RESET );
                        set_tacho_command_inx( snl, TACHO_RESET);
                        DropAndPick(snh, snt, hand, teeth, 1, -6);
                        flag=10;
                        direction = 0;
                        ball = 0;
                    }

                }
                pastSonarValue= sonar_value;
                if(flag == 9){
                    set_tacho_command_inx( snr, TACHO_RESET );
                    set_tacho_command_inx( snl, TACHO_RESET);
                }
                flag = flag + 1;
            }

            flag = 0;
            forward = 0;

            while(flag < 10 && forward <2 && ball ==1){
                //Rotating to left
                moveBobert(snr, snl, rotation, 0, 1, -1, next, 0);
                get_sensor_value0(sn_sonar, &sonar_value ); 
                float pastSonarValue = 0.0; 
                if (flag>0){
                    if ((abs(pastSonarValue-sonar_value)<200) && (abs(pastSonarValue-sonar_value)>120)){
                        set_tacho_command_inx( snr, TACHO_RESET );
                        set_tacho_command_inx( snl, TACHO_RESET);
                        moveBobert(snr, snl, 100, 1, 0, 0, next+1, 0);
                        set_tacho_command_inx( snr, TACHO_RESET );
                        set_tacho_command_inx( snl, TACHO_RESET);
                        forward = forward + 1;
                    }
                    if (sonar_value<105){
                        set_tacho_command_inx( snr, TACHO_RESET );
                        set_tacho_command_inx( snl, TACHO_RESET);
                        DropAndPick(snh, snt, hand, teeth, 1, -6);
                        flag=10;
                        direction = 1;
                        ball = 0;
                    }

                }
                pastSonarValue= sonar_value;
                if(flag == 9){
                    set_tacho_command_inx( snr, TACHO_RESET );
                    set_tacho_command_inx( snl, TACHO_RESET);
                }
                flag = flag + 1;
            }
        }
        set_tacho_command_inx( snr, TACHO_RESET );
        set_tacho_command_inx( snl, TACHO_RESET);
        Sleep(2000);
        int findstraight = 0;
        while(findstraight < 10){
            get_sensor_value0(sn_sonar, &sonar_value ); 
            if(direction == 0){ //robot is on the left
                if((sonar_value<735) && (sonar_value>720)){
                    moveBobert(snr, snl, 180, 0, 1, -1, next, 0);
                }
                if(sonar_value>735){
                    moveBobert(snr, snl, 180, 0, 1, -1, next, 0);
                }
                if((sonar_value<719) && (sonar_value>703)){ //straight
                    findstraight = 11;
                }
                if((sonar_value<703)){
                    moveBobert(snr, snl, 100, -1, 0, 0, next+1, 0);
                }
                set_tacho_command_inx( snr, TACHO_RESET );
                set_tacho_command_inx( snl, TACHO_RESET);

            }
            else if(direction == 1){ //robot is on the right
                if((sonar_value<735) && (sonar_value>720)){
                    moveBobert(snr, snl, 180, 0, -1, 1, next, 0);
                }
                if(sonar_value>735){
                    moveBobert(snr, snl, 180, 0, -1, 1, next, 0);
                }
                if((sonar_value<719) && (sonar_value>703)){ //straight
                    findstraight = 11;
                }
                if((sonar_value<703)){
                    moveBobert(snr, snl, 100, -1, 0, 0, next+1, 0);
                }
                set_tacho_command_inx( snr, TACHO_RESET );
                set_tacho_command_inx( snl, TACHO_RESET);
            }
            
            findstraight = findstraight + 1;

        }

        fflush( stdout );
    }
    Sleep(2000);
    moveBobert(snr, snl, 300, -1, 0, 0, next+1, 0);
    set_tacho_command_inx( snr, TACHO_RESET );
    set_tacho_command_inx( snl, TACHO_RESET);
    Sleep(2000);        
    moveBobert(snr, snl, rotation, 0, 0, -1, next, 150);
    get_sensor_value0(sn_sonar, &sonar_value ); 
    Sleep(2000);
    while(turning < 4){
        moveBobert(snr, snl, rotation, 0, 1, -1, next, 0);
        turning = turning +1;
    }
    set_tacho_command_inx( snr, TACHO_RESET );
    set_tacho_command_inx( snl, TACHO_RESET);
    Sleep(2000);
    get_sensor_value0(sn_sonar, &sonar_value ); 
    moveBobert(snr, snl, rotation, -3, 0, 0, next+1, 0);

    Sleep(3000);
    finisher_left();
    Sleep(2000);
}


void *bluetooth(){
    struct sockaddr_rc addr = { 0 };
    int i=0;
    int statusCheck;

    char string[58];
    // allocate a socket
    s = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

    //set the connection parameters (who to connect to)
    addr.rc_family = AF_BLUETOOTH;
    addr.rc_channel = (uint8_t) 1;
    str2ba (SERV_ADDR, &addr.rc_bdaddr);

    //connect to server
    statusCheck = connect(s, (struct sockaddr *)&addr, sizeof(addr));

    while(i<500){
        if(statusCheck==0){     
            printf("Connected and value is %d\n", statusCheck);
            read_from_server (s, &string, 9);
            printf("%c",string[1]);
        }
        if(statusCheck==1){     
            printf("Disonnected and value is %d\n", statusCheck);
        }

        close(s);
        i = i+1;
    }

}

int read_from_server (int sock, char *buffer, size_t maxSize) {
    int bytes_read = read (sock, buffer, maxSize);
    if (bytes_read <= 0) {
        fprintf (stderr, "Server unexpectedly closed connection...\n");
        close (s);
        exit (EXIT_FAILURE);
    }
    printf ("[DEBUG] received %d bytes\n", bytes_read);
    return bytes_read;
}

int main(void) {
    pthread_t tid[4];
    printf("Creating a thread\n");
    struct sockaddr_rc addr = {
        0
    };
    int statusCheck;

    // allocate a socket
    s = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

    //set the connection parameters (who to connect to)
    addr.rc_family = AF_BLUETOOTH;
    addr.rc_channel = (uint8_t) 1;
    str2ba(SERV_ADDR, & addr.rc_bdaddr);

    //connect to server
    statusCheck = connect(s, (struct sockaddr * ) & addr, sizeof(addr));

    if (statusCheck == 0) {
        char string[58];
        /* Wait for START message */
        read_from_server(s, string, 9);
        if (string[4] == MSG_START) {
            printf("Received start message!\n");
            rank = (unsigned char) string[5]; // 0 == beginner
            side = (unsigned char) string[6]; // 0 == right side
            ally = (unsigned char) string[7];

        }
        if (rank == 0) { /*This is the beginner*/
            if (side == 0) { /*Beginner in Right side*/
                pthread_create( & tid[0], NULL, beginner_right, NULL);
            } else { /*Beginner in Left side*/
                pthread_create( & tid[0], NULL, beginner_left, NULL);
            }
        } else { /*This is the Finisher*/
            if (side == 0) { /*Finisher in Right side*/
                pthread_create( & tid[0], NULL, finisher_right, NULL);
            }
        }
    }
    close(s);
    sleep(5);

    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    exit(0);
    //finisher();
    return 0;

}